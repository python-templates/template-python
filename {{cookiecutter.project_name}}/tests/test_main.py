from {{cookiecutter.package_name}}.__main__ import main


def test_hello_world(capfd):
    main()
    out, _ = capfd.readouterr()
    assert out == "Hello World!\n"
