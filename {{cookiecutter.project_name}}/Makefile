PACKAGE := {{cookiecutter.package_name}}
PACKAGES := $(PACKAGE) tests
DIST_FILES := dist/*.tar.gz dist/*.whl

.phony: default run doctor install format lint test build clean clean-all
default: format lint test build

run: install
	poetry run {{cookiecutter.project_name}}

doctor:
	@ if test ! -d .git; then git init; fi
	poetry run pre-commit install

install:
	@ poetry config settings.virtualenvs.in-project true
	poetry install
	$(MAKE) doctor

format: install
	poetry run isort $(PACKAGES) --recursive --apply
	poetry run black $(PACKAGES)

# TODO: Keep track of Pylint score changes (Eg: fail if lower than previous)
lint:
	poetry run mypy $(PACKAGES) --config-file=.mypy.ini
	poetry run bandit template -f xml -o .reports/bandit.xml -r
	poetry run flake8 template --format=junit-xml --output-file=.reports/flake8.xml

test:
	poetry run pytest

build:
	rm -rf $(DIST_FILES)
	poetry build

clean:
	find $(PACKAGES) -name '__pycache__' -delete
	rm -rf *.egg-info
	rm -rf .cache .pytest .coverage .reports
	rm -rf *.spec dist build

clean-all:
	clean
	rm -rf .venv
