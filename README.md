# Base Python template

## Usage 

Make sure you have [Cookiecutter][1] installed before running. 

```
cookiecutter gl:/python-templates/template-python
```

## Features

- [x] Dependency management and packaging using [Poetry][7]
- [x] Preconfigured code formatting with [Black][2] and [iSort][3]
- [x] Linting with [PyLint][4] and [MyPy][5]
- [x] Testing with[PyTest][6]
- [x] Extensive reports of the linting and testing steps during the build process
- [x] A basic [Gitlab CI][7] Build pipeline with optional deployment to [Heroku][8]
- [x] A [Makefile][9] to provide an easy to remember interface 

[1]: https://github.com/audreyr/cookiecutter
[2]: https://github.com/ambv/black
[3]: https://github.com/timothycrosley/isort
[4]: https://www.pylint.org
[5]: http://mypy-lang.org
[6]: https://pytest.org
[7]: https://about.gitlab.com/product/continuous-integration/
[8]: https://www.heroku.com
[9]: https://www.gnu.org/software/make/